import React from 'react';

//after npm install react-router-dom
import { BrowserRouter as Router, Route } from "react-router-dom";

import ProtectedRoute from './components/protectedRoutes'

//after npm install booststrap --save
import 'bootstrap/dist/css/bootstrap.css'

import Login from './pages/login';
import Error from './pages/Error'
import certificados from './pages/certificados';

function App() {
  return (
    <Router>
      <Route exact path="/" component={ Login }/>
      <Route exact path="/error" component={ Error }/>
      <Route exact path="/certificados" component={ certificados }/>
      <Route exact path="/protegida" render = {() => ProtectedRoute(certificados)}/>
    </Router>
  );
}
export default App;