import React, { Component, Fragment } from 'react'
import LoginForm from '../components/LoginForm'
import Loading from '../components/Loading'
import config from '../config/config'

class Login extends Component {

    state = {
        form:{
            rut: '7777777',
            password: '7777777'
        },
        error: '',
        loading: false
    }

    test(){
        console.log('hola test');
    }

    componentDidMount(){
        console.log('mounted');
    }
    
    handleChange = e => {
        this.setState({
            form: {
                //destructuring, mantener la key existente, crear una nueva
                ...this.state.form,
                [e.target.name] : e.target.value
            }
        })
    }

    handleSubmit = async e => {

        this.setState({
            loading: true,
            error: ''
        })
        
        e.preventDefault();

        try{
            
            let setConfig = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.state.form)
            }
            let response = await fetch(`${ config.url }/auth/login`, setConfig)
            let data = await response.json()
            
            this.setState({
                loading: false
            })
            
            if (response.ok){
                localStorage.setItem('token', data.access_token)
                this.props.history.push('/certificados');  
            }

            // not response OK
            this.setState({
                error: `${ response.statusText}: ${ response.status }`
            })
            console.log(response);

            
        }catch(error){
            this.setState({
                loading: false,
                error: error.message
            })
            console.log(error);
        }
    }

    render() { 
        if (this.state.loading)
            return(
                <Loading/>
            )
        
        return ( 
            <Fragment>
                <LoginForm
                    handleChange = { this.handleChange }
                    handleSubmit = { this.handleSubmit }
                    state = {this.state}
                />
                <div className="container">
                    
                </div>
            </Fragment>       
        )
    }
}
 
export default Login;
