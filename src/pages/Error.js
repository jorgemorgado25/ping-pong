import React from 'react';
import {Link} from 'react-router-dom'

function test(){
    console.log('hola test');
}

const Error = () => {
    return(
        <div className="container">
            <h2>Error</h2>
            <Link className="btn btn-primary" to='/'>Continuar</Link>
            <button onClick={test}>Test</button>
        </div>
    )
}
export default Error