import React from 'react';
import {Redirect} from 'react-router-dom'

const ProtectedRoute = (Component) => {
    return (localStorage.getItem('token'))
        ? <Component />
        : <Redirect to="/" />
    
}
export default ProtectedRoute