import React, { Component } from 'react';

class LoginForm extends Component {

    render() {
        const { handleChange, handleSubmit, state } = this.props
        return ( 
            <div className="container">
                <h1>Login</h1>

                { (state.error) ?
                    <div className="alert alert-danger mt-2">
                        { state.error }
                        </div> 
                    : ''
                }
                <form onSubmit={ handleSubmit }>
                    <div className="form-group">
                        <label>Login</label>
                        <input
                            type="text" 
                            className="form-control"
                            name="rut"
                            onChange={ handleChange }
                            value={ state.form.rut }
                            required
                        />
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input
                            type="text" 
                            className="form-control"
                            name="password"
                            onChange={ handleChange }
                            value={ state.form.password }
                            required
                        />
                    </div>
                    <div className="form-group">
                        <button
                            className="btn btn-primary"
                            type="submit"
                        >Login</button>
                    </div>
                </form>
            </div>
        );
    }
}
 
export default LoginForm;

